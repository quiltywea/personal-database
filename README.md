# Personal Database

A personal database to manage different kind of things.

With Personal Database, you can create databases with tables to manage all kind of things, like your library, music, To-Do's and much more. You can also create lookup fields which join the records of another table.

A simple search function allows to filter by search expressions.

A csv import allows you to import csv files from your device. Personal Database will automatically recognize the delimiter (comma or colon).

# Technical information

This is an HTML-based app. The data is stored in the LocalStorage of the app container. To clear all data:

- Press long until a context menu appears
- Select "Settings"
- Select "Privacy & permissions"
- Select "Clear cache"

# Install

[![OpenStore](https://open-store.io/badges/en_US.svg)](https://open-store.io/app/tcbase.daniel)

# Contribute

- Clone the repo: git clone https://gitlab.com/Schlicki2808/personal-database.git
- Personal Database is a pure HTML/JavaScript app with no dependencies.
- Type ```clickable desktop``` to launch Personal Database locally.

## Contact

Use the issue tracker (https://gitlab.com/Schlicki2808/personal-database/-/issues) if you ran into problems.

## License

Copyright (C) 2022  Daniel Schlieckmann

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
