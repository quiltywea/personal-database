var currentDatabase = null;
var currentTable = null;
var currentField = null;
var currentRecord = null;
var searchValue = "";

function getObjects(name) {
    var jsonString = localStorage.getItem(name);
    if (jsonString != null)
        return JSON.parse(jsonString);
    else
        return null;
}

function putObjects(name, objectList) {
    localStorage.setItem(name, JSON.stringify(objectList));
}

function generateGuid() { 
    var d = new Date().getTime();//Timestamp
    var d2 = (performance && performance.now && (performance.now()*1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;//random number between 0 and 16
        if(d > 0){//Use timestamp until depleted
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        } else {//Use microseconds since page-load if supported
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

function loadDatabases() {
    var databases = getObjects("databases");
    var listview = document.getElementById("databases-list");
    if (databases != null && databases.length > 0) {
        var listviewContent = "";
        for (var i = 0; i < databases.length; i++) {
            listviewContent += "<div class=\"listview-item\"><div style=\"flex-grow:1;padding:8px;\" onclick=\"showTablesView('" + databases[i].Id + "')\"><img class=\"icon\" src=\"/usr/share/icons/suru/actions/scalable/inbox-all.svg\"> " + databases[i].Name + "</div><div><button class=\"btn btn-delete\" onclick=\"deleteDatabase('" + databases[i].Id + "')\"></button></div></div>";
        }
        listview.innerHTML = listviewContent;
    }
    else {
        listview.innerHTML = "<div style=\"padding:12px;\">" + getLoc("empty-databases-message") + "</div>"
    }
}

function createNewDatabase() {
    var name = document.getElementById("modal-input-value").value;
    if (name.length > 0) {
        var newDatabase = { "Id": generateGuid(), "Name": name };
        var databases = getObjects("databases");
        if (databases == null)
            databases = [];
        databases.push(newDatabase);
        putObjects("databases", databases);
        loadDatabases();
        closeModalInput();
    }
}

function deleteDatabase(id) {
    var databases = getObjects("databases");
    var index = databases.findIndex((item) => item.Id == id);
    databases.splice(index, 1);
    putObjects("databases", databases);
    loadDatabases();
}

function renameCurrentDatabase() {
    var name = document.getElementById("modal-input-value").value;
    if (name.length > 0) {
        currentDatabase.Name = name;
        var databases = getObjects("databases");
        var index = databases.findIndex((item) => item.Id == currentDatabase.Id);
        databases[index] = currentDatabase;
        putObjects("databases", databases);
        closeModalInput();
        loadDatabases();
        loadTablesFromDatabase(currentDatabase.Id);
    }
}

function renameCurrentTable() {
    var name = document.getElementById("modal-input-value").value;
    if (name.length > 0) {
        currentTable.Name = name;
        var databases = getObjects("databases");
        var index = databases.findIndex((item) => item.Id == currentDatabase.Id);
        databases[index] = currentDatabase;
        putObjects("databases", databases);
        closeModalInput();
        loadDatabases();
        loadTablesFromDatabase(currentDatabase.Id);
        loadRecordsFromTable();
    }
}

function loadTablesFromDatabase(id) {
    var databases = getObjects("databases");
    var listview = document.getElementById("tables-list");
    currentDatabase = databases.find((item) => item.Id == id);
    document.getElementById("current-database-name").innerText = currentDatabase.Name;
    if (currentDatabase.Tables != null && currentDatabase.Tables.length > 0) {
        var listviewContent = "";
        for (var i = 0; i < currentDatabase.Tables.length; i++) {
            listviewContent += "<div class=\"listview-item\"><div style=\"flex-grow:1;padding:8px;\" onclick=\"showRecords('" + currentDatabase.Tables[i].Id + "')\"><img class=\"icon\" src=\"/usr/share/icons/suru/actions/scalable/view-list-symbolic.svg\"> " + currentDatabase.Tables[i].Name + "</div><div><button class=\"btn btn-delete\" onclick=\"deleteTable('" + currentDatabase.Tables[i].Id + "')\"></button></div></div>";
        }
        listview.innerHTML = listviewContent;
    }
    else {
        listview.innerHTML = "<div style=\"padding:12px;\">" + getLoc("empty-tables-message") + "</div>"
    }
}

function getRecordValueForList(recordId, fieldId, floating) {
    var float = floating ? "float:right;" : "";
    var marginLeft = floating ? "margin-left:auto;" : "";
    var returnValue = "&nbsp;"
    var field = currentTable.Fields.find((item) => item.Id == fieldId);
    var record = currentTable.Records.find((item) => item.Id == recordId);
    var recordValue = "";
    if (record != null) {
        var recordValues = record.Values.find((item) => item.FieldId == fieldId);
        if (recordValues != null)
            recordValue = recordValues.Value;
    }

    if (field.Type == "color") {
        returnValue = "<div style=\"width:40px;height:25px;" + float + "background-color:" + recordValue + ";\">&nbsp;</div>";
    }
    else if (field.Type == "lookup") {
        var table = currentDatabase.Tables.find((item) => item.Id == field.LookupTable);
        if (table != null) {
            var lookupRecord = table.Records.find((item) => item.Id == recordValue);
            if (lookupRecord != null) {
                var lookupRecordValue = lookupRecord.Values.find((item) => item.FieldId == field.LookupValue);
                if (lookupRecordValue != null)
                    returnValue = lookupRecordValue.Value;
            }
        }
    }
    else if (field.Type == "boolean") {
        returnValue = "<div class=\"record-value-container\" style=\"" + marginLeft + "\">"
                    + "<div style=\"margin-right:8px;\">" + field.Name + ":</div>"
                    + "<div>";
        if (recordValue == "true")
            returnValue += "<div style=\"width:25px;height:25px;border:1px solid whitesmoke;" + float + "display:flex;flex-direction:row;align-items:center;justify-content:center;\">&#10004;</div>";
        else
            returnValue += "<div style=\"width:25px;height:25px;border:1px solid whitesmoke;" + float + "\"></div>";
        
        returnValue += "</div></div>";
    }
    else if (field.Type == "image") {
        returnValue = "<div style=\"width:60px;height:40px;" + float + "background-image:url(" + recordValue + ");background-size:contain;background-repeat:no-repeat;background-position:center;\">&nbsp;</div>";
    }
    else {
        returnValue = recordValue;
    }
    return returnValue;
}

function getRecordValuePure(recordIndex, fieldId) {
    var record = currentTable.Records[recordIndex].Values.find((item) => item.FieldId == fieldId);
    if (record != null)
        return record.Value;
    else
        return "";
}

function loadRecordsFromTable() {
    var listview = document.getElementById("records-list");
    document.getElementById("current-table-name").innerText = currentTable.Name;
    document.getElementById("current-table-name-2").innerText = currentTable.Name;

    var listviewContent = "";

    if (currentTable.Fields != null && currentTable.Fields.length > 0) {
        document.getElementById("new-record-button").style.display = "";

        if (currentTable.Records != null && currentTable.Records.length > 0) {
            document.getElementById("btn-sort").style.display = "";
            document.getElementById("btn-find").style.display = "";
    
            var sortFieldId = document.getElementById("sort-by-field").value;
            var sortOrder = document.getElementById("sort-order").value;
            var sortedRecords = [];
            if (sortFieldId != null && sortFieldId != "") {
                var sortField = currentTable.Fields.find((item) => item.Id == sortFieldId);
                for (var i = 0; i < currentTable.Records.length; i++) {
                    sortedRecords.push({ "Id": currentTable.Records[i].Id, "Value": getRecordValuePure(i, sortFieldId) });
                }
                if (sortField.Type == "number" || sortField.Type == "decimal")
                {
                    if (sortOrder == "asc")
                        sortedRecords.sort((a, b) => { return a.Value - b.Value });
                    else
                        sortedRecords.sort((a, b) => { return b.Value - a.Value });
                }
                else {
                    if (sortOrder == "asc")
                        sortedRecords.sort((a, b) => { if (a.Value.toLowerCase() < b.Value.toLowerCase()) return -1; else if (a.Value.toLowerCase() > b.Value.toLowerCase()) return 1; else return 0; });
                    else
                        sortedRecords.sort((a, b) => { if (a.Value.toLowerCase() > b.Value.toLowerCase()) return -1; else if (a.Value.toLowerCase() < b.Value.toLowerCase()) return 1; else return 0; });
                }
            }
            else {
                for (var i = 0; i < currentTable.Records.length; i++) {
                    sortedRecords.push({ "Id": currentTable.Records[i].Id });
                }
            }

            for (var i = 0; i < sortedRecords.length; i++) {
                var showRecord = true;

                if (searchValue.length > 0) {
                    var recordToCheck = currentTable.Records.find((item) => item.Id == sortedRecords[i].Id);
                    showRecord = recordToCheck.Values.some((item) => item.Value.toLowerCase().includes(searchValue.toLowerCase()))
                }

                if (showRecord) {
                    var recordLine = "<div class=\"record-container\">"
                                + "<div class=\"record-column\" onclick=\"editRecord('" + sortedRecords[i].Id + "')\">"
                                + "<div class=\"record-cell\" style=\"font-weight:800;\">";
                    var fieldPos1 = currentTable.Fields.find((item) => item.Visibility == "pos1");
                    if (fieldPos1 != null) {
                        recordLine += getRecordValueForList(sortedRecords[i].Id, fieldPos1.Id, "");
                    }
                    recordLine += "</div>"
                                + "<div class=\"record-cell\">";
                    var fieldPos2 = currentTable.Fields.find((item) => item.Visibility == "pos2");
                    if (fieldPos2 != null) {
                        recordLine += getRecordValueForList(sortedRecords[i].Id, fieldPos2.Id, "");
                    }            
                    recordLine += "</div>"
                                + "</div>"
                                + "<div class=\"record-column\" onclick=\"editRecord('" + sortedRecords[i].Id + "')\">"
                                + "<div class=\"record-cell\" style=\"text-align:right;\">";
                    var fieldPos3 = currentTable.Fields.find((item) => item.Visibility == "pos3");
                    if (fieldPos3 != null) {
                        recordLine += getRecordValueForList(sortedRecords[i].Id, fieldPos3.Id, "float:right;");
                    }            
                    recordLine += "</div>"
                                + "<div class=\"record-cell\" style=\"text-align:right;\">";
                    var fieldPos4 = currentTable.Fields.find((item) => item.Visibility == "pos4");
                    if (fieldPos4 != null) {
                        recordLine += getRecordValueForList(sortedRecords[i].Id, fieldPos4.Id, "float:right;");
                    }
                    recordLine += "</div>"
                                + "</div>"
                                + "<div class=\"record-column\" style=\"flex-grow:unset;min-width:60px;\"><button class=\"btn btn-delete\" onclick=\"deleteRecord('" + sortedRecords[i].Id + "')\"></button></div>"
                                + "</div>";
                    listviewContent += recordLine;
                }
            }
        }
        else {
            document.getElementById("btn-sort").style.display = "none";
            document.getElementById("btn-find").style.display = "none";
            listviewContent = "<div style=\"padding:12px;\">" + getLoc("empty-table-records-message") + "</div>"
        }
    }
    else {
        document.getElementById("new-record-button").style.display = "none";
        document.getElementById("btn-sort").style.display = "none";
        document.getElementById("btn-find").style.display = "none";
        listviewContent = "<div style=\"padding:12px;\">" + getLoc("empty-table-fields-record-message") + "</div>"
    }
    listview.innerHTML = listviewContent;
}

function loadFieldsFromTable() {
    var listview = document.getElementById("fields-list");
    document.getElementById("current-table-name-2").innerText = currentTable.Name;
    if (currentTable.Fields != null && currentTable.Fields.length > 0) {
        var listviewContent = "";
        for (var i = 0; i < currentTable.Fields.length; i++) {
            listviewContent += "<div class=\"listview-item\"><div style=\"flex-grow:1;padding:8px;\" onclick=\"editField('" + currentTable.Fields[i].Id + "')\"><img class=\"icon\" src=\"/usr/share/icons/suru/actions/scalable/note.svg\"> " + currentTable.Fields[i].Name + "</div><div><button class=\"btn btn-delete\" onclick=\"deleteField('" + currentTable.Fields[i].Id + "')\"></button></div></div>";
        }
        listview.innerHTML = listviewContent;
    }
    else {
        listview.innerHTML = "<div style=\"padding:12px;\">" + getLoc("empty-fields-message") + "</div>"
    }
}

function showTablesView(databaseId) {
    showView("tables-selection");
    loadTablesFromDatabase(databaseId);
}

function createNewTable() {
    var name = document.getElementById("modal-input-value").value;
    if (name.length > 0) {
        var newTable = { "Id": generateGuid(), "Name": name };
        if (currentDatabase.Tables == null)
            currentDatabase.Tables = [];
        currentDatabase.Tables.push(newTable);
        var databases = getObjects("databases");
        var index = databases.findIndex((item) => item.Id == currentDatabase.Id);
        databases[index] = currentDatabase;
        putObjects("databases", databases);
        loadTablesFromDatabase(currentDatabase.Id);
        closeModalInput();
    }
}

function deleteTable(id) {
    var indexTab = currentDatabase.Tables.findIndex((item) => item.Id == id);
    currentDatabase.Tables.splice(indexTab, 1);
    var databases = getObjects("databases");
    var indexDb = databases.findIndex((item) => item.Id == currentDatabase.Id);
    databases[indexDb] = currentDatabase;
    putObjects("databases", databases);
    loadTablesFromDatabase(currentDatabase.Id);
}


function showRecords(tableId) {
    currentTable = currentDatabase.Tables.find((item) => item.Id == tableId);
    searchValue = "";
    document.getElementById("sort-by-field").value = "";
    showView("table-records");
    loadRecordsFromTable();
}

function showTableCustomization() {
    showView("table-edit");
    loadFieldsFromTable();
}

function addField() {
    currentField = null;
    document.getElementById("lookup-container").style.display = "none";
    fillLookupTables();
    fillLookupTableFields();
    showModalFieldInput();
    fillTableFieldEdit();
}

function editField(id) {
    currentField = currentTable.Fields.find((item) => item.Id == id);
    if (currentField.Type == "lookup")
        document.getElementById("lookup-container").style.display = "";
    else
        document.getElementById("lookup-container").style.display = "none";
    fillLookupTables();
    fillLookupTableFields();
    showModalFieldInput();
    fillTableFieldEdit();
}

function setLookupFields() {
    if (document.getElementById("table-field-type").value == "lookup")
        document.getElementById("lookup-container").style.display = "";
    else
    document.getElementById("lookup-container").style.display = "none";
}

function fillTableFieldEdit() {
    if (currentField != null) {
        document.getElementById("table-field-name").value = currentField.Name;
        document.getElementById("table-field-type").value = currentField.Type;
        document.getElementById("table-field-lookup-table").value = currentField.LookupTable;
        fillLookupTableFields();
        document.getElementById("table-field-lookup-value").value = currentField.LookupValue;
        document.getElementById("table-field-rule").value = currentField.Rule;
        document.getElementById("table-field-visibility").value = currentField.Visibility;
        document.getElementById("table-field-default").value = currentField.Default;
    }
    else {
        document.getElementById("table-field-name").value = "";
        document.getElementById("table-field-type").value = "text-single";
        document.getElementById("table-field-lookup-table").value = "";
        document.getElementById("table-field-lookup-value").value = "";
        document.getElementById("table-field-rule").value = "";
        document.getElementById("table-field-visibility").value = "";
        document.getElementById("table-field-default").value = "";
    }
}

function fillLookupTables() {
    var lookupSelect = document.getElementById("table-field-lookup-table");

    while (lookupSelect.options.length > 0) {
        lookupSelect.remove(0);
    }

    for (var i = 0; i < currentDatabase.Tables.length; i++) {
        if (currentDatabase.Tables[i].Id != currentTable.Id) {
            var option = document.createElement("option");
            option.text = currentDatabase.Tables[i].Name;
            option.value = currentDatabase.Tables[i].Id;
            lookupSelect.add(option);
        }
    }
}

function fillLookupTableFields() {
    var lookupValueSelect = document.getElementById("table-field-lookup-value");

    while (lookupValueSelect.options.length > 0) {
        lookupValueSelect.remove(0);
    }

    var lookupTableId = document.getElementById("table-field-lookup-table").value;
    if (lookupTableId.length > 0) {
        var table = currentDatabase.Tables.find((item) => item.Id == lookupTableId);
        if (table != null) {
            if (table.Fields != null && table.Fields.length > 0) {
                for (var i = 0; i < table.Fields.length; i++) {
                    var option = document.createElement("option");
                    option.text = table.Fields[i].Name;
                    option.value = table.Fields[i].Id;
                    lookupValueSelect.add(option);    
                }
            }
        }
    }
}

function storeFieldInput() {
    var field = {
        "Name": document.getElementById("table-field-name").value,
        "Type": document.getElementById("table-field-type").value,
        "LookupTable": document.getElementById("table-field-lookup-table").value,
        "LookupValue": document.getElementById("table-field-lookup-value").value,
        "Rule": document.getElementById("table-field-rule").value,
        "Visibility": document.getElementById("table-field-visibility").value,
        "Default": document.getElementById("table-field-default").value
    }

    if (field.Name.length > 0) {
        if (currentField == null) {
            field.Id = generateGuid();
            if (currentTable.Fields == null)
                currentTable.Fields = [];
            currentTable.Fields.push(field);
        }
        else {
            field.Id = currentField.Id;
            var indexField = currentTable.Fields.findIndex((item) => item.Id == field.Id);
            currentTable.Fields[indexField] = field;
        }

        var indexTab = currentDatabase.Tables.findIndex((item) => item.Id == currentTable.Id);
        currentDatabase.Tables[indexTab] = currentTable;
        var databases = getObjects("databases");
        var indexDb = databases.findIndex((item) => item.Id == currentDatabase.Id);
        databases[indexDb] = currentDatabase;
        putObjects("databases", databases);
        loadFieldsFromTable();
        loadRecordsFromTable();
        closeModalFieldInput();
    }
    else {
        document.getElementById("table-field-name").style.border = "2px solid #ff0000";
    }
}

function deleteField(id) {
    var indexField = currentTable.Fields.findIndex((item) => item.Id == id);
    currentTable.Fields.splice(indexField, 1);
    var indexTab = currentDatabase.Tables.findIndex((item) => item.Id == currentTable.Id);
    currentDatabase.Tables[indexTab] = currentTable;
    var databases = getObjects("databases");
    var indexDb = databases.findIndex((item) => item.Id == currentDatabase.Id);
    databases[indexDb] = currentDatabase;
    putObjects("databases", databases);
    loadFieldsFromTable();
    loadRecordsFromTable();
}

function newRecord() {
    currentRecord = null;
    showModalRecordInput();
    fillRecordInput();
}

function getRecordValueForField(fieldId) {
    var field = currentTable.Fields.find((item) => item.Id == fieldId);
    if (currentRecord != null && currentRecord.Values != null && currentRecord.Values.length > 0) {
        var record = currentRecord.Values.find((item) => item.FieldId == fieldId);
        if (record != null) {
            if (field.Type == "boolean") {
                if (record.Value == "true")
                    return "checked";
                else
                    return "";
            }
            else {
                return record.Value;
            }
        }
        else {
            return "";
        }
    }
    else {
        if (field != null)
            return field.Default;
        else
            return "";
    }
}

function fillRecordInput() {
    if (currentTable.Fields != null && currentTable.Fields.length > 0) {
        var modalFields = "";
        for (var i = 0; i < currentTable.Fields.length; i++) {
            var name = currentTable.Fields[i].Name;
            if (currentTable.Fields[i].Rule == "mandatory")
                name += " *";
            modalFields += "<div class=\"modal-field\">"
                         + "<div>" + name + "</div>";
            if (currentTable.Fields[i].Type == "text-single") {
                modalFields += "<input id=\"" + currentTable.Fields[i].Id + "\" type=\"text\" class=\"long-input\" value=\"" + getRecordValueForField(currentTable.Fields[i].Id) + "\">";
            }
            else if (currentTable.Fields[i].Type == "text-multiline") {
                modalFields += "<textarea id=\"" + currentTable.Fields[i].Id + "\" class=\"long-input\">" + getRecordValueForField(currentTable.Fields[i].Id) + "</textarea>";
            }
            else if (currentTable.Fields[i].Type == "number") {
                modalFields += "<input id=\"" + currentTable.Fields[i].Id + "\" type=\"number\" step=\"0\" class=\"long-input\" value=\"" + getRecordValueForField(currentTable.Fields[i].Id) + "\">";
            }
            else if (currentTable.Fields[i].Type == "decimal") {
                modalFields += "<input id=\"" + currentTable.Fields[i].Id + "\" type=\"number\" step=\"0.1\" class=\"long-input\" value=\"" + getRecordValueForField(currentTable.Fields[i].Id) + "\">";
            }
            else if (currentTable.Fields[i].Type == "boolean") {
                modalFields += "<input id=\"" + currentTable.Fields[i].Id + "\" type=\"checkbox\" " + getRecordValueForField(currentTable.Fields[i].Id) + ">";
            }
            else if (currentTable.Fields[i].Type == "date") {
                modalFields += "<input id=\"" + currentTable.Fields[i].Id + "\" type=\"date\" value=\"" + getRecordValueForField(currentTable.Fields[i].Id) + "\">";
            }
            else if (currentTable.Fields[i].Type == "time") {
                modalFields += "<input id=\"" + currentTable.Fields[i].Id + "\" type=\"time\" value=\"" + getRecordValueForField(currentTable.Fields[i].Id) + "\">";
            }
            else if (currentTable.Fields[i].Type == "color") {
                modalFields += "<input id=\"" + currentTable.Fields[i].Id + "\" type=\"color\" value=\"" + getRecordValueForField(currentTable.Fields[i].Id) + "\">";
            }
            else if (currentTable.Fields[i].Type == "lookup") {
                modalFields += "<select id=\"" + currentTable.Fields[i].Id + "\">"
                            + "<option value=\"\"></option>";
                var table = currentDatabase.Tables.find((item) => item.Id == currentTable.Fields[i].LookupTable);
                if (table != null) {
                    for (var j = 0; j < table.Records.length; j++) {
                        var recordValue = table.Records[j].Values.find((item) => item.FieldId == currentTable.Fields[i].LookupValue);
                        if (table.Records[j].Id == getRecordValueForField(currentTable.Fields[i].Id))
                            modalFields += "<option selected value=\"" + table.Records[j].Id + "\">" + recordValue.Value + "</option>"
                        else
                            modalFields += "<option value=\"" + table.Records[j].Id + "\">" + recordValue.Value + "</option>"
                    }
                }
                modalFields += "</select>"
            }
            else if (currentTable.Fields[i].Type == "image") {
                modalFields += "<button onclick=\"selectImageForRecord('" + currentTable.Fields[i].Id + "')\">" + getLoc("select-image") + "</button>";
                var imageValue = getRecordValueForField(currentTable.Fields[i].Id);
                var displayStyle = "";
                if (imageValue == "")
                    displayStyle = "display:none;"
                modalFields += "<div id=\"" + currentTable.Fields[i].Id + "imagecontainer\" class=\"image-container\" data-image=\"" + imageValue + "\" style=\"" + displayStyle + "background-image:url(" + imageValue + ");\"></div>";
            }
            
            modalFields += "</div>";
            
        }
        document.getElementById("modal-record-values").innerHTML = modalFields;
    }
}

function recreateFileInput() {
    var fileInput = document.getElementById("file");
    if (fileInput != null)
        fileInput.parentNode.removeChild(fileInput);
    fileInput = document.createElement("input");
    fileInput.type = "file";
    fileInput.id = "file";
    fileInput.style.display = "none";
    document.body.appendChild(fileInput);
    return fileInput;
}

function selectImageForRecord(fieldId) {
    var fileInput = recreateFileInput();
    fileInput.addEventListener("change", function() { setImage(fieldId); });
    fileInput.click();
}

function prepareImportCsv() {
    var fileInput = recreateFileInput();
    fileInput.addEventListener("change", function() { importCsv(); });
    fileInput.click();
}

function storeRecord() {
    if (currentTable.Fields != null && currentTable.Fields.length > 0) {
        var mandatoryFields = [];
        var record = {
            "Values": []
        };
        for (var i = 0; i < currentTable.Fields.length; i++) {
            var elem = document.getElementById(currentTable.Fields[i].Id);
            var newValue = {
                "FieldId": currentTable.Fields[i].Id,
                "Value": elem != null ? document.getElementById(currentTable.Fields[i].Id).value : ""
            };
            if (currentTable.Fields[i].Type == "boolean") {
                newValue.Value = document.getElementById(currentTable.Fields[i].Id).checked.toString();
            }
            else if (currentTable.Fields[i].Type == "image") {
                newValue.Value = document.getElementById(currentTable.Fields[i].Id + "imagecontainer").dataset.image;
            }
            record.Values.push(newValue);
            if (currentTable.Fields[i].Rule == "mandatory" && newValue.Value.length == 0)
                mandatoryFields.push(currentTable.Fields[i].Id);
        }
        
        if (mandatoryFields.length == 0) {
            if (currentTable.Records == null)
                currentTable.Records = [];

            if (currentRecord == null) {
                record.Id = generateGuid();
                currentTable.Records.push(record);
            }
            else {
                record.Id = currentRecord.Id;
                var indexRec = currentTable.Records.findIndex((item) => item.Id == currentRecord.Id)
                currentTable.Records[indexRec] = record;
            }

            var indexTab = currentDatabase.Tables.findIndex((item) => item.Id == currentTable.Id);
            currentDatabase.Tables[indexTab] = currentTable;
            var databases = getObjects("databases");
            var indexDb = databases.findIndex((item) => item.Id == currentDatabase.Id);
            databases[indexDb] = currentDatabase;
            putObjects("databases", databases);
            loadFieldsFromTable();
            loadRecordsFromTable();
            closeModalRecordInput();
        }
        else {
            for (var i = 0; i < mandatoryFields.length; i++) {
                document.getElementById(mandatoryFields[i]).style.border = "2px solid #ff0000";
            }
        }
    }
}

function deleteRecord(id) {
    var indexRec = currentTable.Records.findIndex((item) => item.Id == id);
    currentTable.Records.splice(indexRec, 1);
    var indexTab = currentDatabase.Tables.findIndex((item) => item.Id == currentTable.Id);
    currentDatabase.Tables[indexTab] = currentTable;
    var databases = getObjects("databases");
    var indexDb = databases.findIndex((item) => item.Id == currentDatabase.Id);
    databases[indexDb] = currentDatabase;
    putObjects("databases", databases);
    loadFieldsFromTable();
    loadRecordsFromTable();
}

function editRecord(id) {
    currentRecord = currentTable.Records.find((item) => item.Id == id);
    showModalRecordInput();
    fillRecordInput();
}

function searchRecords() {
    searchValue = document.getElementById("modal-input-value").value;
    loadRecordsFromTable();
    closeModalInput();
}

function sortRecords() {
    loadRecordsFromTable();
    closeModalSortInput();
}

function setImage(fieldId) {
    var input = document.getElementById("file");
    if (input.files.length > 0) {
        var reader = new FileReader();
        reader.addEventListener("load", function() {
            var elem = document.getElementById(fieldId + "imagecontainer");
            elem.style.backgroundImage = "url(" + reader.result + ")";
            elem.style.display = "";
            elem.dataset.image = reader.result;
        });
        reader.readAsDataURL(input.files[0]);
    }
}

function getCsvCell(originalValue) {
    var resultValue = originalValue;
    if (resultValue != null && resultValue != "") {
        if (resultValue[0] == "\"")
            resultValue = resultValue.substring(1, resultValue.length);
        if (resultValue[resultValue.length - 1] == "\"")
            resultValue = resultValue.substring(0, resultValue.length - 1);
        return resultValue;
    }
    else {
        return "";
    }
}

function cleanLineDelimiter(line, delimiter) {
    var work = true;
    var resultLine = "";
    for (var i = 0; i < line.length; i++) {
        var addChar = line[i];
        if (line[i] == "\"")
            work = !work;
        if (work) {
            if (line[i] == delimiter)
                addChar = "µ";
        }
        resultLine += addChar;
    }
    return resultLine;
}

function importCsv() {
    var input = document.getElementById("file");
    if (input.files.length > 0) {
        var reader = new FileReader();
        reader.addEventListener("load", function() {
            var input = document.getElementById("file");
            var delimiter = ",";
            var checkComma = reader.result.split(",").length;
            var checkColon = reader.result.split(";").length;
            if (checkColon > checkComma)
                delimiter = ";";
            var lines = reader.result.replace(/\r/g, "").split("\n");
            var newTable = { "Id": generateGuid(), "Name": input.files[0].name.replace(".csv", ""), "Fields": [], "Records": [] };
            for (var i = 0; i < lines.length; i++) {
                var line = cleanLineDelimiter(lines[i], delimiter);
                var lineItems = line.split("µ");
                if (i == 0) {
                    for (var j = 0; j < lineItems.length; j++) {
                        var visibility = j < 4 ? "pos" + (j + 1) : "";
                        var newField = {
                            "Id": generateGuid(),
                            "Name": getCsvCell(lineItems[j]),
                            "Type": "text-single",
                            "LookupTable": "",
                            "LookupValue": "",
                            "Rule": "",
                            "Visibility": visibility,
                            "Default": ""
                        }
                        newTable.Fields.push(newField);
                    }
                }
                else {
                    if (lines[i].length > 0) {
                        var newRecord = { "Id": generateGuid(), "Values": [] };
                        for (var j = 0; j < lineItems.length; j++) {
                            newRecord.Values.push({ "FieldId": newTable.Fields[j].Id, "Value": getCsvCell(lineItems[j])});
                        }
                        newTable.Records.push(newRecord);
                    }
                }
            }
            currentDatabase.Tables.push(newTable);
            var databases = getObjects("databases");
            var index = databases.findIndex((item) => item.Id == currentDatabase.Id);
            databases[index] = currentDatabase;
            putObjects("databases", databases);
            loadTablesFromDatabase(currentDatabase.Id);    
        });
        reader.readAsText(input.files[0]);
    }
}