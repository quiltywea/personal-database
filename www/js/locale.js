var locale = "en";

const translations = {
    "en": {
        "databases": "Databases",
        "new-database-prompt": "Name of new database:",
        "tables": "Tables",
        "database-pre": "Database:",
        "rename-database-prompt": "New name of database:",
        "new-table-prompt": "Name of new table:",
        "records": "Records",
        "table-pre": "Table:",
        "rename-table-prompt": "New name of table:",
        "search-prompt": "Search for:",
        "table-custom": "Edit table fields",
        "ok": "OK",
        "cancel": "Cancel",
        "field-name-md": "Field name *",
        "field-type": "Field type",
        "lookup-table": "Lookup table",
        "lookup-display-value": "Lookup field value",
        "rule": "Rule",
        "visibility": "Visibility",
        "default-value": "Default value",
        "text-single": "Text (single line)",
        "text-multiline": "Text (multi line)",
        "number": "Number",
        "decimal": "Number with decimals",
        "boolean": "Checkbox",
        "date": "Date",
        "time": "Time",
        "color": "Color",
        "lookup": "Lookup",
        "image": "Image",
        "none": "None",
        "mandatory": "Mandatory",
        "pos1": "Position 1 (title)",
        "pos2": "Position 2 (subtitle)",
        "pos3": "Position 3",
        "pos4": "Position 4",
        "empty-databases-message": "It's empty here. Click on + button on the bottom right to create a new database.",
        "empty-tables-message": "It's empty here. Click on + button on the bottom right to create a new table.",
        "empty-fields-message": "It's empty here. Click on + button on the bottom right to create a new field.",
        "empty-table-records-message": "It's empty here. Click on + button on the bottom right to create a new record.",
        "empty-table-fields-record-message": "This table has still no fields. Click on the settings button in the upper right corner to create some fields.",
        "sort-by-field": "Sort by field",
        "sort-order": "Sort order",
        "ascending": "Ascending",
        "descending": "Descending",
        "no-sorting": "No sorting",
        "select-image": "Select image",
    },
    "de": {
        "databases": "Datenbanken",
        "new-database-prompt": "Name der neuen Datenbank:",
        "tables": "Tabellen",
        "database-pre": "Datenbank:",
        "rename-database-prompt": "Neuer Name für die Datenbank:",
        "new-table-prompt": "Name der neuen Tabelle:",
        "records": "Datensätze",
        "table-pre": "Tabelle:",
        "rename-table-prompt": "Neuer Name für die Tabelle:",
        "search-prompt": "Suchen nach:",
        "table-custom": "Tabellenfelder bearbeiten",
        "ok": "OK",
        "cancel": "Abbrechen",
        "field-name-md": "Feldname *",
        "field-type": "Feldtyp",
        "lookup-table": "Lookup-Tabelle",
        "lookup-display-value": "Lookup-Feld",
        "rule": "Regel",
        "visibility": "Sichtbarkeit",
        "default-value": "Standardwert",
        "text-single": "Text (einzelne Zeile)",
        "text-multiline": "Text (mehrere Zeilen)",
        "number": "Ganze Zahl",
        "decimal": "Dezimalzahl",
        "boolean": "Checkbox",
        "date": "Datum",
        "time": "Zeit",
        "color": "Farbe",
        "lookup": "Lookup",
        "image": "Bild",
        "none": "Ohne",
        "mandatory": "Pflichtfeld",
        "pos1": "Position 1 (Titel)",
        "pos2": "Position 2 (Untertitel)",
        "pos3": "Position 3",
        "pos4": "Position 4",
        "empty-databases-message": "Es ist leer hier. Klicke auf + unten rechts, um eine neue Datenbank anzulegen.",
        "empty-tables-message": "Es ist leer hier. Klicke auf + unten rechts, um eine neue Tabelle anzulegen.",
        "empty-fields-message": "Es ist leer hier. Klicke auf + unten rechts, um ein neues Feld anzulegen.",
        "empty-table-records-message": "Es ist leer hier. Klicke auf + unten rechts, um einen neuen Datensatz anzulegen.",
        "empty-table-fields-record-message": "Diese Tabelle hat noch keine Felder. Klick auf die Einstellungen-Schaltfläche oben rechts, um ein paar Felder anzulegen.",
        "sort-by-field": "Sortieren nach Feld",
        "sort-order": "Sortierfolge",
        "ascending": "Aufsteigend",
        "descending": "Absteigend",
        "no-sorting": "Keine Sortierung",
        "select-image": "Bild auswählen",
    }
}

function setLanguage() {
    var lang = navigator.language || navigator.userLanguage;
    if (lang.indexOf("-") > -1) {
        lang = lang.substring(0, 2);
    }
    if (translations[lang] != null)
        locale = lang.toLowerCase();
    else
        locale = "en";
}


// When the page content is ready...
document.addEventListener("DOMContentLoaded", () => {
    setLanguage();
    document
        // Find all elements that have the key attribute
        .querySelectorAll("[data-i18n-key]")
        .forEach(translateElement);
});

// Replace the inner text of the given HTML element
// with the translation in the active locale,
// corresponding to the element's data-i18n-key
function translateElement(element) {
    const key = element.getAttribute("data-i18n-key");
    const translation = translations[locale][key];
    element.innerText = translation;
}

function getLoc(key) {
    const translation = translations[locale][key];
    return translation;
}