var viewHistory = [ "databases-selection" ];
var modalInputOkEvent;

function showView(viewName, addToHistory = true) {
    if (addToHistory)
        viewHistory.push(viewName);
    
    var backButton = document.getElementById("back-button");
    if (viewHistory.length > 1)
        backButton.style.display = "";
    else
        backButton.style.display = "none";
    
    var items = document.getElementsByClassName("content");
    for (var i = 0; i < items.length; i++) {
        if (items[i].id == viewName) {
            items[i].style.display = "";
            document.getElementById("top-text").innerText = getLoc(items[i].getAttribute("data-title"));
        }
        else {
            items[i].style.display = "none";
        }
    }
}

function showElement(id) {
    document.getElementById(id).style.display = '';
}

function showModalInput(prompt, defaultValue, okEvent) {
    modalInputOkEvent = okEvent;
    document.getElementById("modal-prompt").innerText = prompt;
    document.getElementById("modal-input-value").value = defaultValue;
    document.getElementById("ok-button").addEventListener("click", okEvent);
    document.getElementById("modal-input").style.top = "-4px";
    document.getElementById("modal-input-value").focus();
    document.getElementById("modal-input-value").setSelectionRange(0, document.getElementById("modal-input-value").value.length);
}

function closeModalInput() {
    document.getElementById("ok-button").removeEventListener("click", modalInputOkEvent);
    document.getElementById("modal-input").style.top = "-500px";
}

function showModalFieldInput() {
    document.getElementById("modal-table-field").style.top = "-4px";
    document.getElementById("table-field-name").style.border = "";
    document.getElementById("table-field-name").focus();
}

function closeModalFieldInput() {
    document.getElementById("modal-table-field").style.top = "-1500px";
}

function showModalRecordInput() {
    document.getElementById("modal-record").style.top = "-4px";
}

function closeModalRecordInput() {
    document.getElementById("modal-record").style.top = "-1500px";
}

function showModalSortInput() {
    var sortSelect = document.getElementById("sort-by-field");

    while (sortSelect.options.length > 0) {
        sortSelect.remove(0);
    }

    if (currentTable.Fields != null && currentTable.Fields.length > 0) {
        var optionFirst = document.createElement("option");
        optionFirst.text = getLoc("no-sorting");
        optionFirst.value = "";
        sortSelect.add(optionFirst);    

        for (var i = 0; i < currentTable.Fields.length; i++) {
            var option = document.createElement("option");
            option.text = currentTable.Fields[i].Name;
            option.value = currentTable.Fields[i].Id;
            sortSelect.add(option);    
        }
    }

    document.getElementById("sort-order-container").style.display = "none";
    document.getElementById("modal-table-sort").style.top = "-4px";
}

function closeModalSortInput() {
    document.getElementById("modal-table-sort").style.top = "-1500px";
}

function hideElement(id) {
    document.getElementById(id).style.display = 'none';
}

function goBack() {
    if (viewHistory.length > 1) {
        viewHistory.pop();
        var newView = viewHistory[viewHistory.length - 1];
        showView(newView, false);
        document.getElementById("btn-back").blur();
    }
}

function setSortOrderContainer() {
    var sortFieldId = document.getElementById("sort-by-field").value;
    var sortOrderContainer = document.getElementById("sort-order-container");
    if (sortFieldId == "")
        sortOrderContainer.style.display = "none";
    else
        sortOrderContainer.style.display = "";
}